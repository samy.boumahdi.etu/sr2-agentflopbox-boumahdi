# SR2-AgentFlopBox-Boumahdi

	- Author : Samy Boumahdi
	
# Introduction


L'objectif du projet est désormais de développer une application cliente pour la plate-forme FlopBox qui permette de synchroniser les données stockées à distance dans un ensemble de serveurs FTP avec le système de fichiers local d'une machine sur laquelle l'application cliente sera exécutée.


# Commentaires 


### Lancer le projet

Tout d'abords lancer le projet Flopbox (ajout à la racine du projet) avec mvn package (dans le répertoire FlopBox) puis java -jar target/simple-service-1.0-SNAPSHOT-jar-with-dependencies.jar 

Ajouter un serveur sur Flopbox par exemple : curl --location --request POST 'http://localhost:8080/flopbox/ftp.ubuntu.com'

Lui associer un alias : curl --location --request PUT 'http://localhost:8080/flopbox/ftp.ubuntu.com' --header 'alias: alias1'

Maintenant dans le repertoire Projet 2, faire : mvn package assembly:single puis java -jar target/Projet2-1.0-SNAPSHOT-jar-with-dependencies.jar 

### Problèmes rencontrés

Je n'ai pas su et pu tester mes méthodes deleteFile et UploadFile.
Je n'ai pas eu le temps de faire les méthodes d'update de fichiers.

# Architecture

### Throw / Catch :

	- IOException : Signale qu'une erreur a été détecté mais ne donne pas plus d'information concernant l'erreur.

	- ParseException : Signale qu'une erreur a été atteinte de manière inattendue lors de l'analyse c'est à dire à l'utilisation de la méthode parse de Java.

	- RuntimeException : Signale qu'une opération non prévisible a eu lieu dans notre cas si la réponse à notre requête ne vaut pas 200.


# Code Samples

Méthode qui permet de télécharger un dossier ou un fichier disponible sur le server ftp présent sur Flopbox. Le téléchargement commencera avec pour racine le "path" passé en paramètre. 

	public void downloadDir(AppClient appClient, String alias, String path, String localPath ,String username, String password, int port) throws IOException, ParseException 
	{
        String [] arbo = list(appClient, alias, path, username, password, port);
        for (String file : arbo) {
            String[] fileParse = Helper.parse(file);
            String newPath = path+"/"+fileParse[1];
            if(fileParse[0].equals("d")){
                String newLocalPath = localPath + fileParse[1];
                Path p = Paths.get(newLocalPath);
                Files.createDirectories(p);
                downloadDir(appClient, alias, newPath, newLocalPath, username,  password, port);
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fileParse[4]+" "+fileParse[5]);
                Files.setLastModifiedTime(p, FileTime.fromMillis(date.getTime()));
            } else{
                Path p = Paths.get(localPath);
                downloadFile(appClient, alias, newPath, localPath , username,  password, port);
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fileParse[4]+" "+fileParse[5]);
                Files.setLastModifiedTime(p, FileTime.fromMillis(date.getTime()));
                }
            }
        }`
