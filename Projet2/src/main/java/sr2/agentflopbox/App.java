package sr2.agentflopbox;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;

/**
 * Author : Samy Boumahdi
 * Main class
 */
public class App 
{
    public static void main( String[] args ) throws IOException, ParseException {
        System.out.println( "Hello World!" );
        ActionFolder actionFolder = new ActionFolder();
        AppClient appClient = new AppClient();
        String [] tmp = appClient.allAlias().split(",");
        appClient.createAllFolders(tmp);
        //System.out.println(Arrays.toString(actionFolder.list(appClient, "alias1", "/", "anonymous", "anonymous", 21)));
        //actionFolder.downloadFile(appClient, "alias1", "cloud-images/FOOTER.html", System.getProperty("user.dir"), "anonymous", "anonymous", 21);
        actionFolder.downloadDir(appClient, "alias1/", "cloud-images", System.getProperty("user.dir")+"/alias1/", "anonymous", "anonymous", 21);
        //actionFolder.upload(appClient, "alias1/", "", new File("toto.txt"), "anonymous", "anonymous", 21);
        //actionFolder.deleteFile(appClient, "alias1/", "cloud-images/","FOOTER.html", "anonymous", "anonymous", 21);
    }
}
