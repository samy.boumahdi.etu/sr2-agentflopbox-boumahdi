package sr2.agentflopbox;

import java.util.ArrayList;
import java.util.List;

public class Helper {

    /**
     * Function which parse all informations of a file or directory to retrieve only important infromations (name, date) and more easily
     * @param detail the detail of informations about a file or directory
     * @return an array where every elements of this array is the infromation of a file or a directory
     */
    public static String[] parse(String detail){
        List<Folder> list = new ArrayList<Folder>();
        String[] tab = detail.split(" ");
        Folder f = new Folder();
        String name;
        f.setDate(tab[4] + " " + tab[5]);
        name = tab[1];
        f.setName(name);
        list.add(f);
        return tab;
    }

}
