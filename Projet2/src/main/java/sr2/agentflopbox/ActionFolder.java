package sr2.agentflopbox;

import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Class of differents actions on directories and folders of a server at distance on the plateform FlopBox
 */
public class ActionFolder {


    /**
     * Function which list informations about the tree view from the path of a server on Flopbox
     * @param appClient
     * @param alias the alias which represents a ftp server on Flopbox
     * @param path display of the tree view from the path
     * @param username the username to access the server remotely
     * @param password the password to access the server remotely
     * @param port the port by default on Flopbox
     * @return an array with all informations (type, name, date) for each file or directory of the tree view
     */
    public String[] list(AppClient appClient, String alias, String path, String username, String password, int port) {
        try {
            HttpGet httpGet = new HttpGet("http://localhost:8080/flopbox/" + alias + path);
            httpGet.setHeader("Content-Type", "text/plain");
            httpGet.setHeader("username", username);
            httpGet.setHeader("password", password);
            httpGet.setHeader("port", String.valueOf(port));
            System.out.println(httpGet);

            HttpResponse response = null;
            String res = "";
            response = appClient.httpclient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() != 200){
                throw new RuntimeException("error");
            }
            res = EntityUtils.toString(response.getEntity());
            return res.split("\n");

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }


    /**
     * Function which download a file from an ftp server on Flopbox
     * @param appClient
     * @param alias the alias which represents a ftp server on Flopbox
     * @param path download a file from the path
     * @param localPath the localPath to locally store the downloaded file
     * @param username the username to access the server remotely
     * @param password the password to access the server remotely
     * @param port the port by default on Flopbox
     */
    public void downloadFile(AppClient appClient, String alias, String path, String localPath ,String username, String password, int port){
        try {
            HttpGet httpGet = new HttpGet("http://localhost:8080/flopbox/"+alias+"file/"+path);
            httpGet.setHeader("Accept", "application/octet-stream");
            httpGet.setHeader("username", username);
            httpGet.setHeader("password", password);
            httpGet.setHeader("port", String.valueOf(port));
            HttpResponse response = null;
            response = appClient.httpclient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() != 200){
                //throw new RuntimeException("error");
            }
            System.out.println(response);
            HttpEntity entity = response.getEntity();
            String filename = path.substring(path.lastIndexOf("/") + 1);
            try (InputStream inputStream = entity.getContent()) {
                Files.copy(inputStream, Paths.get(localPath, filename), StandardCopyOption.REPLACE_EXISTING);
            }
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }


    /**
     * Function which download directories and folders from an ftp server on Flopbox
     * @param appClient
     * @param alias the alias which represents a ftp server on Flopbox
     * @param path download repositories and files from the path
     * @param localPath the localPath to locally store the downloaded repositories and files
     * @param username the username to access the server remotely
     * @param password the password to access the server remotely
     * @param port the port by default on Flopbox
     * @throws IOException
     * @throws ParseException
     */
    public void downloadDir(AppClient appClient, String alias, String path, String localPath ,String username, String password, int port) throws IOException, ParseException {
        String [] arbo = list(appClient, alias, path, username, password, port);
        for (String file : arbo) {
            String[] fileParse = Helper.parse(file);
            String newPath = path+"/"+fileParse[1];
            if(fileParse[0].equals("d")){
                String newLocalPath = localPath + fileParse[1];
                Path p = Paths.get(newLocalPath);
                Files.createDirectories(p);
                downloadDir(appClient, alias, newPath, newLocalPath, username,  password, port);
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fileParse[4]+" "+fileParse[5]);
                Files.setLastModifiedTime(p, FileTime.fromMillis(date.getTime()));
            } else{
                Path p = Paths.get(localPath);
                downloadFile(appClient, alias, newPath, localPath , username,  password, port);
                Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(fileParse[4]+" "+fileParse[5]);
                Files.setLastModifiedTime(p, FileTime.fromMillis(date.getTime()));
            }
        }
    }

    /**
     * Function which delete a file locally and put him on a new folder named deleted on the ftp server
     * @param appClient
     * @param alias the alias which represents an ftp server on Flopbox
     * @param path the path of the file to delete
     * @param file the file to delete
     * @param username the username to access the server remotely
     * @param password the password to access the server remotely
     * @param port the port by default on Flopbox
     */
    public void deleteFile(AppClient appClient, String alias, String path, String file, String username, String password, int port) {
        HttpPut httpPut = new HttpPut("http://localhost:8080/flopbox/"+alias+"file/"+path+file);
        httpPut.setHeader("Content-Type", "text/plain");
        httpPut.setHeader("username", username);
        httpPut.setHeader("password", password);
        httpPut.setHeader("port", String.valueOf(port));
        httpPut.setHeader("rename", "/.deleted/"+file);
        try {
            appClient.httpclient.execute(httpPut);
        }catch (IOException e){
        }

    }

    /**
     * Function which upload the file create locally
     * @param appClient
     * @param alias the alias which represents an ftp server on Flopbox
     * @param path where uplaod the file
     * @param file the file create locally
     * @param username the username to access the server remotely
     * @param password the password to access the server remotely
     * @param port the port by default on Flopbox
     */
    public static void upload(AppClient appClient, String alias, String path, File file, String username, String password, int port){
        HttpPost httpPost = new HttpPost("http://localhost:8080/flopbox/"+alias+"file/"+path);
        httpPost.setHeader("Content-Type", "text/plain");
        httpPost.setHeader("username", username);
        httpPost.setHeader("password", password);
        httpPost.setHeader("port", String.valueOf(port));
        try {
            FilePart fp = new FilePart("file", file);
            Part[] parts = { fp };
            httpPost.setEntity((HttpEntity) new MultipartRequestEntity(parts, (HttpMethodParams) httpPost.getParams()));
            appClient.httpclient.execute(httpPost);
        } catch (IOException e) {
        }
    }

}
