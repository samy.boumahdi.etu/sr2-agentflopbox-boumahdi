package sr2.agentflopbox;


import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;

public class AppClient {

    HttpClient httpclient = new DefaultHttpClient();

    /**
     * Function which indicate all alias associate at an ftp server on flopbox
     * @return all alias
     */
    public String allAlias() {
        HttpGet httpget = new HttpGet("http://localhost:8080/flopbox/alias/all");
        HttpResponse response = null;
        String res = "";
        try {
            response = this.httpclient.execute(httpget);
            if (response.getStatusLine().getStatusCode() != 200){
                throw new RuntimeException("error");
            }
            res = EntityUtils.toString(response.getEntity());
            return res;
        } catch (IOException e) {
            throw new RuntimeException();
        }
    }


    /**
     * Function which create all folders locally in function of alias presents on Flopbox
     * @param allAlias an array of every alias presents on Flopbox
     */
    public void createAllFolders(String [] allAlias){
        for (int i=0; i < allAlias.length - 1; i++) {
            File file = new File(allAlias[i]);
            file.mkdir();
            System.out.println("Dossier " + allAlias[i] + " crée.");
        }
    }

}