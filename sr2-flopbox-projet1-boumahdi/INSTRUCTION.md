### Ajouter un serveur: 

curl --location --request POST 'http://localhost:8080/flopbox/ftp.ubuntu.com'

curl --location --request POST 'http://localhost:8080/flopbox/webtp.fil.univ-lille1.fr'


### Mettre un alias:

curl --location --request PUT 'http://localhost:8080/flopbox/ftp.ubuntu.com' --header 'alias: alias1'

curl --location --request PUT 'http://localhost:8080/flopbox/webtp.fil.univ-lille1.fr' --header 'alias: alias2'

### Récuperer le nom du serveur :

curl --location --request GET 'http://localhost:8080/flopbox/alias1'

curl --location --request GET 'http://localhost:8080/flopbox/alias2'


### List :

curl --location --request GET 'http://localhost:8080/flopbox/alias1/' --header 'username: anonymous' --header 'password: 1234' --header 'port: 21'


curl --location --request GET 'http://localhost:8080/flopbox/alias2/' --header 'username: nguyen' --header 'password: aezffa521' --header 'port: 21'


### Download de fichier :

curl --location --request GET 'http://localhost:8080/flopbox/alias1/file/cloud-images/FOOTER.html' --header 'username: anonymous' --header 'password: 1234' --header 'port: 21' -o TEST1.txt

curl --location --request GET 'http://localhost:8080/flopbox/alias2/file/TEST.txt' --header 'username: nguyen' --header 'password: aze54f' --header 'port: 21' -o TEST2.txt


### Upload de fichier :
(Dans le dossier du fichier)

curl -v -X POST -F file=@image.jpg  --header 'username: nguyen' --header 'password: azed51zae' http://localhost:8080/flopbox/alias2/file/image.jpg


### Rename file:

curl -v -X PUT --header 'rename: image2.jpg'  --header 'username: nguyen' --header 'password: zae8d574' http://localhost:8080/flopbox/alias2/file/image.jpg

Rename folder :
curl -v -X PUT --header 'rename: nini'  --header 'username: nguyen' --header 'password: cq651d' http://localhost:8080/flopbox/alias2/directory/nounou


### Delete folder :

curl -v -X DELETE --header 'username: nguyen' --header 'password: tbzr561' http://localhost:8080/flopbox/alias2/directory/releases


### Create folder : 

curl -v -X POST --header 'username: nguyen' --header 'password: aze5f41' http://localhost:8080/flopbox/alias2/directory/coucou


