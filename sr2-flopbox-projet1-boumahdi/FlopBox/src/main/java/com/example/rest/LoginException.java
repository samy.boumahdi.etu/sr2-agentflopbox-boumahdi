package com.example.rest;

/**
 * @author Kevin Nguyen
 */

public class LoginException extends Exception {

	public LoginException(String msg) {
		super(msg);
	}
}
