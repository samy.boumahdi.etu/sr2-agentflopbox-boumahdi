package com.example.rest;

/**
 * @author Kevin Nguyen
 */

public class ListException extends Exception {

	public ListException(String msg) {
		super(msg);
	}

}
