package com.example.rest;

/**
 * @author Kevin Nguyen
 */

public class LogoutException extends Exception {

	public LogoutException(String msg) {
		super(msg);
	}
}
